#import <Foundation/Foundation.h>
#include "CoreServices.h"

BOOL openApp(NSString* bundleID) {
    BOOL success = [[LSApplicationWorkspace defaultWorkspace] openApplicationWithBundleID:bundleID];
    return success;
}