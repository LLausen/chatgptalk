import Foundation

class ChatGPT {
    private let apiKey: String
    private let baseURL: String = "https://api.openai.com/v1/chat/completions"
    @Published public var chat: [[String: String]] = []
    private var tokens: [Int] = []
    
    public static let shared = ChatGPT()

    private init() {
        self.apiKey = key
    }

    func reset() {
        self.chat = []
    }

    func prompt(_ message: String) throws -> (String?, (String, [String: Any])?) {
        let semaphore: DispatchSemaphore = DispatchSemaphore(value: 0)

        let functions: [[String: Any]] = 
        [
            [
                "type": "function",
                "function": [
                    "name": "open_app",
                    "description": "Opens the specified app on the users device",
                    "parameters": [
                        "type": "object",
                        "properties": [ 
                            "bundleID": [
                                "type": "string",
                                "description": "iOS bundle identifier of the requested app"
                            ]
                        ],
                        "required": ["bundleID"]
                    ]
                ],
            ],
            [
                "type": "function",
                "function": [
                    "name": "send_mail",
                    "description": "Sends email with specified content to specified mail address",
                    "parameters": [
                        "type": "object",
                        "properties": [
                            "mail": [
                                "type": "string",
                                "description": "recipient mail address"
                            ],
                            "content": [
                                "type": "string",
                                "description": "html formatted content of the email"
                            ]
                        ],
                        "required": ["mail", "content"]
                    ]
                ]
            ]
        ]
        
        guard let url: URL = URL(string: baseURL) else {
            throw NSError(domain: "Invalid URL", code: 0, userInfo: nil)
        }

        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        chat.append(["role": "user", "content": message])
        let parameters: [String: Any] = [
            "model": "gpt-3.5-turbo",
            "messages": chat,
            "temperature": 0.7,
            "tools": functions,
        ]
        request.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        var err: Error?
        var result: String?
        var function: (String, [String: Any])?
        let task: URLSessionDataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error: Error = error {
                err = error
                semaphore.signal()
                return
            }

            if let data: Data = data {
                do {
                    if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                       if let choices = jsonResponse["choices"] as? [[String: Any]],
                          let usage = jsonResponse["usage"] as? [String: Any],
                          let promptTokens = usage["prompt_tokens"] as? Int,
                          let completionTokens = usage["completion_tokens"] as? Int,
                          let response = choices.first,
                          let message = response["message"] as? [String: Any] {
                            self.tokens.append(promptTokens)
                            self.tokens.append(completionTokens)
                            if let text: String = message["content"] as? String {
                                self.chat.append(["role": "assistant", "content": text])
                                result = text
                            }
                            if let tools = message["tool_calls"] as? [[String: Any]],
                               let tool = tools.first {
                                if let type = tool["type"] as? String,
                                   type == "function",
                                   let fun = tool["function"] as? [String: Any],
                                   let name = fun["name"] as? String,
                                   let content = fun["arguments"] as? String,
                                   let data = content.data(using: .utf8),
                                   let jsonContent = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                                    function = (name, jsonContent)
                                }
                            }
                        } else {
                            err = NSError(domain: "Invalid response format \(jsonResponse)", code: 0, userInfo: nil)
                        }
                    } else {
                        err = NSError(domain: "Invalid response format \(data)", code: 0, userInfo: nil)
                    }
                } catch {
                    err = error
                }
                semaphore.signal()
            }
        }
        task.resume()
        semaphore.wait()
        if let error: Error = err {
            throw error
        } else if result == nil && function == nil {
            throw NSError(domain: "Unexpected error!", code: 0, userInfo: nil)
        } else {
            return (result, function)
        }
    }

    func getTokens() -> Int {
        return self.tokens.sum()
    }
}

extension Sequence where Element: AdditiveArithmetic {
    func sum() -> Element { reduce(.zero, +) }
}