import SwiftUI
import UIKit
import Foundation
import AVFoundation

struct RootView: View {
    @State var prevText: String = ""
    @State var error: String = ""
    @State var isRunning: Bool = false
    @State var isRecording: Bool = false
    @State var isSpeaking: Bool = false
    @ObservedObject var keywordRecognizer = KeywordRecognizer(key: "los geht's")
    @ObservedObject var textRecognizer = SpokenTextRecognizer()


	var body: some View {
        List {
            ForEach(ChatGPT.shared.chat.reversed(), id: \.self) { section in
                Section(header: Text("\(section["role"] ?? "")")) {
                    Text("\(section["content"] ?? "")")
                }
            }
        }
        .listStyle(InsetGroupedListStyle())
        Spacer()
        HStack {
            if !isRunning {
                Button("ChatGPT fragen") {
                    keywordRecognizer.stopListening()
                    self.run()
                }
                    .padding()
                    .foregroundColor(.white)
                    .background(Color.blue)
                    .cornerRadius(10)
            } else if isRecording {
                Button("ChatGPT hört zu...", action: self.run)
                    .padding()
                    .foregroundColor(.white)
                    .background(Color.red)
                    .cornerRadius(10)
                    .disabled(true)
            } else if isSpeaking {
                Button("ChatGPT spricht...", action: self.run)
                    .padding()
                    .foregroundColor(.white)
                    .background(Color.green)
                    .cornerRadius(10)
                    .disabled(true)
            } else {
                Button("ChatGPT denkt nach...", action: self.run)
                    .padding()
                    .foregroundColor(.white)
                    .background(Color.orange)
                    .cornerRadius(10)
                    .disabled(true)
            }
            Spacer()
            Text(keywordRecognizer.isListening ? "Listening..." : "Not listening")
            .padding()
            .onAppear {
                keywordRecognizer.onKeywordDetected = { [self] in
                    DispatchQueue.main.async { self.run() }
                }
            }
            Spacer()
            Button("reset") {
                ChatGPT.shared.reset()
            }
            .padding()
            .foregroundColor(.white)
            .background(Color.blue)
            .cornerRadius(10)
        }
    }

    func run() {
        guard !isRunning else {
            return
        }
        let time: Double = 3.0
        isRunning = true
        isRecording = true
        textRecognizer.startRecognition(TimeInterval(time), callback: self.callback)
    }

    func callback() {
        isRecording = false
        let request = textRecognizer.spokenText
        if request == self.prevText { 
            resumeKeywordRecognizer()
            return
        }
        self.prevText = request
        DispatchQueue.global(qos: .userInitiated).async {
            var text: String?
            var function: (String, [String: Any])?
            do {
                (text, function) = try ChatGPT.shared.prompt(request)
            } catch {
                resumeKeywordRecognizer()
                return
            }
            if let function = function {
                let json = function.1
                if function.0 == "open_app" {
                    if let bundleID: String = json["bundleID"] as? String {
                        if !openApp(bundleID) {
                            error = "\(bundleID) ist nicht installiert!"
                        }
                    }
                } else if function.0 == "send_mail" {
                    if let mail: String = json["mail"] as? String , let content: String = json["content"] as? String {
                        UIApplication.shared.open(createMailtoURL(recipient: mail, subject: "", body: content)!)
                    }
                }
                resumeKeywordRecognizer()
            } else if let text = text {
                DispatchQueue.main.async { self.isSpeaking = true }
                SpeechSynthesizer().speakText(text, completion: self.resumeKeywordRecognizer)
            }
        }
    }

    func resumeKeywordRecognizer() {
        DispatchQueue.main.async {
            self.isSpeaking = false
            self.isRunning = false
        }
        try? keywordRecognizer.startListening()
    }

    func createMailtoURL(recipient: String, subject: String, body: String) -> URL? {
        let encodedRecipient: String = recipient.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let encodedSubject: String = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let encodedBody: String = body.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

        let urlString: String = "mailto:\(encodedRecipient)?subject=\(encodedSubject)&body=\(encodedBody)"

        return URL(string: urlString)
    }
}