import SwiftUI

@main
struct ChatGPTalk: App {
    var body: some Scene {
        WindowGroup {
            RootView()
        }
    }
}