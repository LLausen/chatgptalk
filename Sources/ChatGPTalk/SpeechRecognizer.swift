import Speech
import AVFoundation
import Combine

class KeywordRecognizer: ObservableObject {
    private let audioEngine = AVAudioEngine()
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "de-DE"))
    private let request = SFSpeechAudioBufferRecognitionRequest()
    private var recognitionTask: SFSpeechRecognitionTask?
    private let listenKeyword: String
    
    @Published var isListening = false
    
    var onKeywordDetected: (() -> Void)?
    
    init(key: String) {
        self.listenKeyword = key
        request.taskHint = .dictation
        request.requiresOnDeviceRecognition = true
        SFSpeechRecognizer.requestAuthorization { authStatus in
            if authStatus == .authorized {
                do {
                    try self.startListening()
                } catch {
                    return
                }
            }
        }
    }
    
    func startListening() throws {
        let inputNode = audioEngine.inputNode
        request.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer?.recognitionTask(with: request) { [weak self] result, error in
            guard let self = self else { return }
            
            var isFinal: Bool = false
            
            if let result = result {
                let recognizedText = result.bestTranscription.formattedString
                if recognizedText.lowercased().contains(self.listenKeyword) {
                    self.onKeywordDetected?()
                    isFinal = true
                }
            }
            
            if error != nil || isFinal {
                self.stopListening()
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        
        audioEngine.prepare()
        try audioEngine.start()
        DispatchQueue.main.async {
            self.isListening = true
        }
    }
    
    func stopListening() {
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
        
        recognitionTask?.cancel()
        recognitionTask = nil
        DispatchQueue.main.async {
            self.isListening = false
        }
    }
}


class SpokenTextRecognizer: ObservableObject {
    private var audioEngine = AVAudioEngine()
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "de-DE"))
    private var recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
    private var recognitionTask: SFSpeechRecognitionTask?
    private var timer: Timer?
    private var lastAudioInputTime: Date?
    @Published var spokenText: String = ""

    init() {
        speechRecognizer?.defaultTaskHint = .dictation
    }

    func startRecognition(_ seconds: TimeInterval, callback: (() -> Void)? = nil) {
        audioEngine = AVAudioEngine()
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        lastAudioInputTime = nil

        let inputNode = audioEngine.inputNode

        recognitionTask?.cancel()
        recognitionTask = nil

        recognitionRequest.shouldReportPartialResults = true

        let audioFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: audioFormat) { buffer, _ in
            self.recognitionRequest.append(buffer)
        }

        audioEngine.prepare()

        do {
            try audioEngine.start()

            timer = Timer.scheduledTimer(withTimeInterval: seconds, repeats: true) { _ in
                guard let lastInputTime = self.lastAudioInputTime else {
                    self.stopRecognition()
                    if let callback: (() -> Void) = callback { callback() }
                    return
                }
                let currentTime = Date()
                let timeDifference = currentTime.timeIntervalSince(lastInputTime)
                if timeDifference >= seconds {
                    self.stopRecognition()
                    if let callback: (() -> Void) = callback { callback() }
                }
            }

            recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest) { result, error in
                if let result = result {
                    DispatchQueue.main.async {
                        self.spokenText = result.bestTranscription.formattedString
                        self.lastAudioInputTime = Date()
                    }
                }

                if let error = error {
                    print("Speech recognition error: \(error.localizedDescription)")
                }
            }
        } catch {
            print("Audio engine error: \(error.localizedDescription)")
        }
    }

    func stopRecognition() {
        audioEngine.stop()
        recognitionRequest.endAudio()
        timer?.invalidate()
        timer = nil
    }
}