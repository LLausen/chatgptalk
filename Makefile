TARGET = iphone:clang:latest:15.0
ARCHS = arm64
THEOS_PACKAGE_SCHEME=rootless

include $(THEOS)/makefiles/common.mk

APPLICATION_NAME = ChatGPTalk
ChatGPTalk_SWIFT_BRIDGING_HEADER = Sources/ChatGPT-Bridging-Header.h
ChatGPTalk_FILES = Sources/ChatGPTalk/*.swift Sources/ChatGPTalkC/wrapper.m 
ChatGPTalk_PRIVATE_FRAMEWORKS = MobileCoreServices

include $(THEOS_MAKE_PATH)/application.mk

before-package::
	@ldid -Sent.xml $(THEOS_STAGING_DIR)/Applications/ChatGPTalk.app
